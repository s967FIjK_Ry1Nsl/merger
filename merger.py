#!/usr/bin/env python
import getopt
import os
import re
import requests
import sys
import urlparse

from ConfigParser import ConfigParser
from lxml import etree
from requests_oauthlib import OAuth1
from StringIO import StringIO

def indent(msg, indent=2):
    return '\n'.join(' ' * indent + l for l in msg.splitlines())

class PRException(Exception):
    pass

class RequestException(Exception):
    def __init__(self, resp):
        self.resp = resp
        super(RequestException, self).__init__(
            'Request failed: %d: %s' % (resp.status_code, resp.text))

class RepoDeletedException(PRException):
    def __init__(self):
        super(RepoDeletedException, self).__init__(
            "We can't merge your pull request because you deleted your "
            "repository.")

class SourcePermissionException(PRException):
    def __init__(self):
        super(SourcePermissionException, self).__init__(
            'Your pull request cannot be merged as you made your fork private.')

class BadContentException(PRException):
    def __init__(self, message):
        message = ("We can't merge your pull request as there are some "
                   "problems with the HTML.\n"
                   "Please don't add extra tags, CSS classes, div's, or "
                   "anything other than what is used in the existing quotes.\n"
                   "\n"
                   "Guru meditation: %s" % message)
        super(BadContentException, self).__init__(message)

class InvalidFilesException(PRException):
    def __init__(self):
        super(InvalidFilesException, self).__init__(
            "Please don't edit any files other than editme.html")

class ConflictedException(PRException):
    def __init__(self):
        super(ConflictedException, self).__init__(
            'Your pull request has conflicts and cannot be merged. Conflicts '
            'are the result of other people editing the same files in the '
            'same location.')

class RedirectingOAuthSession(object):
    """Because:
    https://github.com/requests/requests-oauthlib/issues/94
    """
    def __init__(self, key, secret):
        self.session = requests.Session()
        self.session.auth = OAuth1(key, secret)

    def get(self, url, **kwargs):
        kwargs['allow_redirects'] = False
        for i in xrange(10):
            resp = self.session.get(url, **kwargs)
            if resp.status_code in (301, 302):
                url = resp.headers.get('Location')
                if url:
                    continue
            return resp
        raise requests.TooManyRedirects

    def post(self, url, data=None, **kwargs):
        return self.session.post(url, data=data, **kwargs)

class Merger(object):
    regex = re.compile(r'^diff ((?!editme\.html).)*$')
    usage = """Automatically merges all open pull requests for a repository.

Usage: $ %s [OPTION] repo-url

    -c --credentials    path to OAuth credentials file
    -d --dryrun         don't actually merge or reject anything
    -h, --help          print this message and exit.
""" % sys.argv[0]

    def parse_args(self, args):
        self.dryrun = False

        key, secret = None, None
        options, base_url = getopt.getopt(args, 'hdc:',
                                          ['help', 'dryrun', 'credentials='])
        for opt, arg in options:
            if opt in ('-c', '--credentials'):
                cfg = ConfigParser()
                if cfg.read(arg):
                    key = cfg.get('oauth', 'key')
                    secret = cfg.get('oauth', 'secret')
                else:
                    print >> sys.stderr, 'Invalid path:', arg
                    exit(1)
            elif opt in ('-d', '--dryrun'):
                self.dryrun = True
            elif opt in ('-h', '--help'):
                print self.usage
                exit(0)
        if not all((key, secret, base_url)):
            print >> sys.stderr, self.usage
            exit(1)

        self.session = RedirectingOAuthSession(key, secret)
        parts = urlparse.urlsplit(base_url[0])
        self.full_name = parts.path.strip('/')
        self.api_base = urlparse.urlunsplit((
            parts.scheme,
            'api.' + parts.netloc, '', '', ''))
        self.pr_base = (self.api_base + '/2.0/repositories/' +
                        parts.path.strip('/') + '/pullrequests/')

    def run(self):
        url = self.pr_base
        while url:
            resp = self.session.get(url)
            if resp.ok:
                body = resp.json()
                for pr in body['values']:
                    try:
                        self.merge(pr, 'Nice work!')
                        print 'Merged', pr['links']['html']['href'], '--', pr['title'].encode('utf-8')
                        print indent(self.diff(pr)).encode('utf-8')
                    except RequestException, e:
                        print >> sys.stderr, e.resp.url, 'failed:', e.resp.status_code, e.resp.text
                    except PRException, e:
                        try:
                            self.decline(pr, str(e))
                        except (PRException, RequestException), _e:
                            print >> sys.stderr, 'Error:', str(_e)
                        else:
                            print 'Declined', pr['links']['html']['href'], '--', pr['title'].encode('utf-8')
                            print indent(str(e)).encode('utf-8')
                            print
                            print indent(self.diff(pr)).encode('utf-8')

                url = body.get('next')
            else:
                print >> sys.stderr, url, 'failed:', ('authorization failed' if
                                                      resp.status_code == 401 else
                                                      resp.text.encode('utf-8'))
                break


    def decline(self, pr, message):
        if not self.dryrun:
            resp = self.session.post(pr['links']['decline']['href'],
                                     {'message': message})
            if not resp.ok:
                raise RequestException(resp)

    def loadfile(self, repo_fullname, rev, path):
        resp = self.session.get('%s/1.0/repositories/%s/src/%s/%s' %
                                (self.api_base, repo_fullname, rev, path))
        if not resp.ok:
            raise RequestException(resp)
        else:
            try:
                return resp.json()['data']
            except ValueError:
                raise RequestException(resp)

    def ensure_html(self, html):
        schema_doc = etree.parse(os.path.join(os.path.dirname(__file__),
                                              'editme.xsd'))
        schema = etree.XMLSchema(schema_doc)

        try:
            doc = etree.parse(StringIO(html))
        except etree.XMLSyntaxError, e:
            raise BadContentException(str(e.error_log.last_error))
        else:
            if not schema.validate(doc):
                raise BadContentException(str(schema.error_log.last_error))

    def diff(self, pr):
        resp = self.session.get(pr['links']['diff']['href'])
        if not resp.ok:
            raise RequestException(resp)
        return resp.text

    def ensure_validity(self, pr):
        if pr['source'].get('repository') is None:
            raise RepoDeletedException

        # ensure no additional files are touched
        if any((l for l in self.diff(pr).splitlines() if self.regex.match(l))):
            raise InvalidFilesException

        # test HTML syntax
        try:
            html = self.loadfile(pr['source']['repository']['full_name'],
                                 pr['source']['commit']['hash'],
                                 'editme.html')
        except RequestException, re:
            if (pr['source']['repository']['full_name'] != self.full_name and
                    re.resp.status_code in (401, 403)):
                raise SourcePermissionException
            else:
                raise
        else:
            self.ensure_html(html)

    def merge(self, pr, message):
        self.ensure_validity(pr)
        if self.dryrun:
            return

        resp = self.session.post(pr['links']['merge']['href'], {'message': message})
        if (resp.status_code == 400 and 'Conflicts during merge.' in
                                           resp.json()['error']['message']):
            # ignore merge conflicts, nothing we can do about them
            raise ConflictedException
        elif not resp.ok:
            raise RequestException(resp)

if __name__ == '__main__':
    merger = Merger()
    merger.parse_args(sys.argv[1:])
    merger.run()
